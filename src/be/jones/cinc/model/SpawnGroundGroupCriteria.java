package be.jones.cinc.model;

import be.jones.cinc.client.model.Country;
import be.jones.cinc.client.model.UnitTemplate;
import be.jones.cinc.client.model.UnitTemplate.GroupCategory;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.Collections;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import org.hildan.fxgson.FxGson;

public class SpawnGroundGroupCriteria {

  private static Gson gson = FxGson.coreBuilder()
      .registerTypeAdapter(SpawnGroundGroupCriteria.class,
          new SpawnGroundGroupCriteria().new GroupSerializer())
      .registerTypeAdapter(SpawnUnitCriteria.class,
          new SpawnGroundGroupCriteria().new UnitSerializer())
      .disableHtmlEscaping()
      .create();
  public ObjectProperty<Country> country = new SimpleObjectProperty<>();
  public StringProperty name = new SimpleStringProperty();
  public ListProperty<SpawnUnitCriteria> units = new SimpleListProperty<>(
      FXCollections.observableArrayList(Collections.emptyList()));
  public SpawnGroundGroupCriteria() {
  }

  public String toString() {
    return gson.toJson(this);
  }

  public class GroupSerializer implements JsonSerializer<SpawnGroundGroupCriteria> {

    public GroupSerializer() {
      super();
    }

    @Override
    public JsonElement serialize(final SpawnGroundGroupCriteria criteria, final Type type,
        final JsonSerializationContext context) {

      JsonObject mainCriteria = new JsonObject();
      mainCriteria.add("country_id", context.serialize(criteria.country.getValue().getId()));
      mainCriteria.add("group_category", context.serialize(GroupCategory.GROUND_UNIT.getValue()));

      JsonObject groupData = new JsonObject();
      groupData.add("name", context.serialize(criteria.name));
      groupData.add("task", context.serialize("Ground Nothing"));

      JsonArray units = new JsonArray();
      for (SpawnUnitCriteria unit : criteria.units) {
        if (unit.type.getValue() != null) {
          units.add(context.serialize(unit));
        }
      }

      groupData.add("units", units);
      mainCriteria.add("group_data", groupData);

      return mainCriteria;
    }
  }

  public class UnitSerializer implements JsonSerializer<SpawnUnitCriteria> {

    public UnitSerializer() {
      super();
    }

    @Override
    public JsonElement serialize(final SpawnUnitCriteria criteria, final Type type,
        final JsonSerializationContext context) {

      JsonObject jsonUnitCriteria = new JsonObject();
      jsonUnitCriteria.add("x", context.serialize(new BigDecimal(criteria.latitude.getValue())));
      jsonUnitCriteria.add("y", context.serialize(new BigDecimal(criteria.longitude.getValue())));
      jsonUnitCriteria.add("type", context.serialize(criteria.type.getValue().getName()));
      jsonUnitCriteria.add("name", context.serialize(criteria.name));

      return jsonUnitCriteria;
    }
  }

  public class SpawnUnitCriteria {

    public ObjectProperty<UnitTemplate> type = new SimpleObjectProperty<>();
    public StringProperty name = new SimpleStringProperty();
    public StringProperty skill = new SimpleStringProperty();
    public StringProperty latitude = new SimpleStringProperty();
    public StringProperty longitude = new SimpleStringProperty();
    public IntegerProperty heading = new SimpleIntegerProperty();

  }


}
