package be.jones.cinc.controllers;

import be.jones.cinc.client.CincClient;
import java.net.InetAddress;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class ServerConnectionDialogController {

  @FXML
  private TextField serverAddress;

  @FXML
  private TextField serverPort;

  @FXML
  private void initialize() {
  }

  public void connectToServer(ActionEvent event) {
    try {
      System.out
          .println("Connecting to server " + serverAddress.getText() + ":" + serverPort.getText());

      CincClient.getInstance().connect(
          InetAddress.getByName(serverAddress.getText()),
          Integer.parseInt(serverPort.getText())
      );
      System.out
          .println("Connected to server " + serverAddress.getText() + ":" + serverPort.getText());

      Node source = (Node) event.getSource();
      Stage stage = (Stage) source.getScene().getWindow();
      stage.close();

    } catch (Exception e) {
      Alert alert = new Alert(Alert.AlertType.ERROR);
      alert.setHeaderText("Could not connect to server");
      alert.setContentText(e.getMessage());
      alert.showAndWait();
    }
  }
}
