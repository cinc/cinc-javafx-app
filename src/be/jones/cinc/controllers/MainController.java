package be.jones.cinc.controllers;

import be.jones.cinc.client.CincClient;
import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.javascript.object.LatLong;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class MainController {

  @FXML
  private MenuItem menuConnect;
  @FXML
  private MenuItem menuDisconnect;

  @FXML
  private AnchorPane connectionDialog;
  @FXML
  private ServerConnectionDialogController serverConnectionDialogController;
  private Stage connectionDialogStage = new Stage();

  @FXML
  private GoogleMapView map;
  @FXML
  private MapController mapController;

  @FXML
  private void initialize() {
    Scene scene = new Scene(connectionDialog);
    connectionDialogStage.setScene(scene);
    connectionDialogStage.initModality(Modality.APPLICATION_MODAL);
    setupMainMenu();
  }

  protected void setupMainMenu() {
    menuConnect.setOnAction(e -> {
      connectionDialogStage.showAndWait();
      enableNetworkReliantUI();
    });

    menuDisconnect.setOnAction(e -> {
      CincClient.getInstance().disconnect();
      disableNetworkReliantUI();
    });
  }

  private void enableNetworkReliantUI() {
    if (CincClient.getInstance().isConnected()) {
      menuConnect.setDisable(true);
      menuDisconnect.setDisable(false);
      mapController.enableContextMenuNetworkMenuItems();
    }
  }

  private void disableNetworkReliantUI() {
    menuConnect.setDisable(false);
    menuDisconnect.setDisable(true);
    mapController.disableContextMenuNetworkMenuItems();
  }
}
