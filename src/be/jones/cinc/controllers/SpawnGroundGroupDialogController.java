package be.jones.cinc.controllers;

import be.jones.cinc.client.commands.SpawnGroupCommand;
import be.jones.cinc.client.dao.CountryDAO;
import be.jones.cinc.client.model.Country;
import be.jones.cinc.client.model.UnitTemplate;
import be.jones.cinc.model.SpawnGroundGroupCriteria;
import com.lynden.gmapsfx.javascript.object.LatLong;
import java.text.DecimalFormat;
import java.util.Collection;
import java.util.Collections;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Pagination;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;
import javafx.util.Callback;

public class SpawnGroundGroupDialogController {

  static private DecimalFormat formatter = new DecimalFormat("###.00000");
  private static Collection<Country> countries = Collections.emptyList();
  public LatLong latlong;
  @FXML
  private ComboBox<Country> countrySelect;
  @FXML
  private ComboBox<UnitTemplate.Category> unitCategorySelect;
  @FXML
  private ComboBox<UnitTemplate> unitTypeSelect;
  @FXML
  public TextField unitLatitudeInput;
  @FXML
  public TextField unitLongitudeInput;

  @FXML
  private TextField unitNameInput;

  @FXML
  private Spinner<Integer> unitHeadingSelect;

  @FXML
  private TextField groupNameInput;
  private SpawnGroundGroupCriteria criteria = new SpawnGroundGroupCriteria();

  @FXML
  private Pagination unitPaginator;

  @FXML
  public void initialize() {
    for (int i = 0; i < 20; i++) {
      criteria.units.add(i, criteria.new SpawnUnitCriteria());
    }
  }

  public void onShow() {
    populateDropdowns();

    unitPaginator.currentPageIndexProperty().addListener((obs, oldValue, newValue) -> {
      updateUnitForm(newValue.intValue());
    });

    groupNameInput.textProperty().addListener(
        (obs, oldValue, newValue) -> criteria.name.setValue(newValue));

    countrySelect.valueProperty().addListener(
        (obs, oldValue, newValue) -> criteria.country.setValue(newValue));

    unitNameInput.textProperty().addListener(
        (obs, oldValue, newValue) -> criteria.units.get(unitPaginator.getCurrentPageIndex()).name
            .setValue(newValue));

    unitLatitudeInput.textProperty().addListener((obs, oldValue, newValue) -> criteria.units
        .get(unitPaginator.getCurrentPageIndex()).latitude.setValue(newValue));

    unitLongitudeInput.textProperty().addListener((obs, oldValue, newValue) -> criteria.units
        .get(unitPaginator.getCurrentPageIndex()).longitude.setValue(newValue));

    unitTypeSelect.valueProperty().addListener((observableValue, unitTemplate, newUnitTemplate) -> criteria.units
        .get(unitPaginator.getCurrentPageIndex()).type.setValue(newUnitTemplate));
  }

  private void updateUnitForm(int index) {
    unitLatitudeInput.setText(criteria.units.get(index).latitude.getValue());
    unitLongitudeInput.setText(criteria.units.get(index).longitude.getValue());
    unitNameInput.setText(criteria.units.get(index).name.getValue());
    unitTypeSelect.setValue(criteria.units.get(index).type.getValue());
    unitCategorySelect.setValue(criteria.units.get(index).type.getValue().getCategory());
  }

  @FXML
  public void populateCountrySelect() {
    if (countries.isEmpty()) {
      System.out.println("Populating Country Dropdown");
      countries = CountryDAO.listAll();
      ObservableList countriesList = FXCollections.observableArrayList(countries);
      countrySelect.setItems(countriesList);

      javafx.util.Callback<ListView<Country>, ListCell<Country>> countryCellFactory = new Callback<ListView<Country>, ListCell<Country>>() {

        @Override
        public ListCell<Country> call(ListView<Country> p) {

          final ListCell<Country> cell = new ListCell<Country>() {

            @Override
            protected void updateItem(Country t, boolean bln) {
              super.updateItem(t, bln);

              if (t != null) {
                setText(t.getName());
              } else {
                setText(null);
              }
            }
          };
          return cell;
        }
      };
      countrySelect.setCellFactory(countryCellFactory);
      countrySelect.setButtonCell(countryCellFactory.call(null));
    }
  }

  @FXML
  public void populateUnitCategorySelect() {
    // We are not including trains because they seem to be a special case that is not
    // fully implemented
    ObservableList groundUnits = FXCollections.observableArrayList(
        UnitTemplate.Category.AIR_DEFENCE,
        UnitTemplate.Category.ARMOR,
        UnitTemplate.Category.ARTILLERY,
        UnitTemplate.Category.INFANTRY,
        UnitTemplate.Category.UNARMED);
    unitCategorySelect.setItems(groundUnits);
  }

  @FXML
  public void setCountry() {
    criteria.country.setValue(countrySelect.getValue());
    populateCountryToe();
  }

  @FXML
  public void setGroupName() {
    criteria.name.setValue(groupNameInput.getText());
  }

  @FXML
  public void populateDropdowns() {
    populateCountrySelect();
    populateUnitCategorySelect();

    javafx.util.Callback<ListView<UnitTemplate>, ListCell<UnitTemplate>> unitCellFactory = new Callback<ListView<UnitTemplate>, ListCell<UnitTemplate>>() {

      @Override
      public ListCell<UnitTemplate> call(ListView<UnitTemplate> p) {

        final ListCell<UnitTemplate> cell = new ListCell<UnitTemplate>() {

          @Override
          protected void updateItem(UnitTemplate t, boolean bln) {
            super.updateItem(t, bln);

            if (t != null) {
              setText(t.getName());
            } else {
              setText(null);
            }
          }
        };
        return cell;
      }
    };
    unitTypeSelect.setCellFactory(unitCellFactory);
    unitTypeSelect.setButtonCell(unitCellFactory.call(null));
  }

  @FXML
  private void populateCountryToe() {
    for (Country country : countries) {
      if (countrySelect.getValue().getId() == country.getId()) {
        if (country.getToe() == null) {
          country.getToe();
        }
      }
    }
    populateUnitSelect();
  }

  @FXML
  void spawnGroup() {
    System.out.println("Spawning Group");
    System.out.println(criteria.toString());
    SpawnGroupCommand.spawnGroup(criteria.toString());
  }

  @FXML
  public void populateUnitSelect() {
    System.out.println("populateUnitSelect called");
    Country country = countrySelect.getValue();
    UnitTemplate.Category granularCategory = unitCategorySelect.getValue();

    if (country != null && granularCategory != null) {
      System.out.println("Setting Unit Type");
      Collection<UnitTemplate> units = FXCollections
          .observableArrayList(country.getToe().getUnits().get(granularCategory));
      unitTypeSelect.setItems(FXCollections.observableArrayList(units));
      unitTypeSelect.setVisibleRowCount(units.size());
    }
    unitTypeSelect.setValue(null);
  }
}
