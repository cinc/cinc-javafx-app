package be.jones.cinc.controllers;

import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.javascript.event.GMapMouseEvent;
import com.lynden.gmapsfx.javascript.event.UIEventType;
import com.lynden.gmapsfx.javascript.object.GoogleMap;
import com.lynden.gmapsfx.javascript.object.LatLong;
import com.lynden.gmapsfx.javascript.object.MapOptions;
import com.lynden.gmapsfx.javascript.object.MapTypeIdEnum;
import com.sun.glass.ui.Robot;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class MapController {

  @FXML
  private GoogleMapView mainMapView;

  @FXML
  private AnchorPane spawnGroundGroupDialog;

  @FXML
  private SpawnGroundGroupDialogController spawnGroundGroupDialogController;

  private Stage spawnGroundGroupDialogStage;

  private GoogleMap map;

  private ContextMenu contextMenu = new ContextMenu();
  private MenuItem spawnGroupMenuItem = new MenuItem();

  @FXML
  public void initialize() {
    mainMapView.addMapInializedListener(this::configureMap);

    spawnGroundGroupDialogStage = new Stage();
    spawnGroundGroupDialogStage.setAlwaysOnTop(true);
    spawnGroundGroupDialogStage.toFront();
    spawnGroundGroupDialogStage.setScene(new Scene(spawnGroundGroupDialog));
    spawnGroundGroupDialogStage.setOnShowing(event -> spawnGroundGroupDialogController.onShow());
  }

  private void configureMap() {
    MapOptions mapOptions = new MapOptions();

    mapOptions.center(new LatLong(42.4600000, 42.08000))
        .mapType(MapTypeIdEnum.ROADMAP)
        .zoom(9)
        .streetViewControl(false);
    map = mainMapView.createMap(mapOptions, false);

    setupContextMenu();
  }

  private void setupContextMenu() {

    spawnGroupMenuItem.setDisable(false);
    spawnGroupMenuItem.setText("Add ground units");
    spawnGroupMenuItem.setOnAction(actionEvent -> {
      spawnGroundGroupDialogController.latlong = (LatLong) contextMenu.getUserData();
      spawnGroundGroupDialogStage.show();
    });

    contextMenu.getItems().addAll(spawnGroupMenuItem);

    map.addMouseEventHandler(UIEventType.rightclick, (GMapMouseEvent event) -> {
      /* We are doing this because the GoogleMap GMapMouseEvent is not the same as
         a native JavaFX MouseEvent and does not contain things like the current
         position of the mouse. Since we want to show a context menu in the location
         of the click we need to get it. Unfortunately the only way I have found to
         do this so far is using the Robot API which is very not recommended and could
         break in later versions of Java. But it works for now so roll with it.
      */
      Robot robot = com.sun.glass.ui.Application.GetApplication().createRobot();
      double x = robot.getMouseX();
      double y = robot.getMouseY();

      contextMenu.setUserData(event.getLatLong());
      contextMenu.show(mainMapView.getWebview().getScene().getWindow(), x, y);
    });

    map.addMouseEventHandler(UIEventType.click, (GMapMouseEvent event) -> {
      spawnGroundGroupDialogController.unitLatitudeInput
          .setText(Double.toString(event.getLatLong().getLatitude()));
      spawnGroundGroupDialogController.unitLongitudeInput
          .setText(Double.toString(event.getLatLong().getLongitude()));

    });
  }

  public void enableContextMenuNetworkMenuItems() {
    spawnGroupMenuItem.setDisable(false);
  }

  public void disableContextMenuNetworkMenuItems() {
    spawnGroupMenuItem.setDisable(true);
  }
}
