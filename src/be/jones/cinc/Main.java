package be.jones.cinc;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    Parent root = FXMLLoader.load(getClass().getResource("resources/Main.fxml"));
    primaryStage.setTitle("Commander In Chief");
    primaryStage.setScene(new Scene(root, 1024, 768));
    primaryStage.setMaximized(true);
    primaryStage.setAlwaysOnTop(false);
    primaryStage.show();
  }
}