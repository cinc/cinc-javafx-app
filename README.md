# Commander In Chief

A GUI that allows you to interact with a running mission inside Eagle Dynamic's
DigitalCombatSimulator.

Current status: Experimental, written by someone *very* new to JavaFX with no unit tests
and is basically non-functional for any real use.

Currently we can:

* Connect to the server
* Spawn a group of ground units on the map with basic data

Depends on GMapsFX (Development dependency resolution to be setup)
